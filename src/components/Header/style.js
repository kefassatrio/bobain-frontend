import styled from "styled-components";

export const HeaderContainer = styled.div`
.header
{
	background-color: var(--background);
	display: flex;
	justify-content: space-between;
	left: 0;
	margin-bottom: 10px;
    position: fixed;
    z-index: 1;
	top: 0;
    width: 100%;
    box-shadow: 5px 0px 5px rgba(0, 0, 0, 0.2);
}
.header-cart
{
	width: 90%;
}
.header-cart-container
{
	align-items: center;
	background-color: var(--semi-dark);
	cursor: pointer;
	display: flex;
	height: 50px;
	justify-content: center;
	margin-right: 20px;
	width: 50px;
}
.header-cart-container:active
{
	background-color: var(--semi-dark);
	transition: 0s;
}
.header-cart-container:hover
{
	background-color: var(--dark);
	transition: ease-in-out 0.3s;
}
.header-title
{
	font-size: 2.5em;
	margin-left: 20px;
	margin-top: 5px;
	color: var(--black);
}

@media only screen and (max-width: 500px)
{
	.header-title
	{
		font-size: 2em;
		margin-left: 15px;
    }
}
`