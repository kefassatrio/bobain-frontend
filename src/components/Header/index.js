import React, { Component } from 'react';
import cart from '../../assets/img/cart.png';
import { HeaderContainer } from './style';
import { Link } from "react-router-dom";

class Header extends Component {
    render() {
        return (
            <HeaderContainer>
                <header className="header">
                    <Link to="/" className="no-decor">
                        <h1 className="header-title">
                            Boba.in        
                        </h1>
                    </Link>
                    <Link to="/cart" className="no-decor">
                    <div className="header-cart-container">
                        <img src={cart} alt="cart" className="header-cart"/>
                    </div>
                    </Link>
                </header>
            </HeaderContainer>
        );
    }
}

export default Header;
