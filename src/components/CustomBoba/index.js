import React, { Component } from 'react';
import { CustomBobaContainer } from './style';
import Header from '../Header';
import LoadingScreen from '../LoadingScreen';
import axios from 'axios';
import black_bubble from '../../assets/img/Black Bubble.png';
import cheese from '../../assets/img/Cheese.png';
import ice from '../../assets/img/Ice.png';
import brown_sugar from '../../assets/img/Brown Sugar.png';
import milk from '../../assets/img/Milk.png';
import clear from '../../assets/img/Clear.png';
import tea from '../../assets/img/Tea.png';

class CustomBoba extends Component {
    constructor(){
        super();
        this.state = {
            currentBobaObject: {},
            selectedBoba: "Clear Boba",
            selectedBobaCost: 10000,
            selectedToppings: [],
            selectedToppingsCost: 0,
            allSelectedBoba: [],
            currentNumberOfBoba : 1,
            bobaId : 1,
            isLoading: false
        }

    }
    componentDidMount(){
        const myUrl = "https://bobain.herokuapp.com"

        axios.get(`${myUrl}/rest-api/current-boba`)
        .then(res => {
            this.setState({ currentBobaObject : res.data});
            console.log(res.data)
        }
        );
    }


    render() {
        const {allSelectedBoba, selectedToppings, selectedBoba, currentNumberOfBoba, bobaId, selectedToppingsCost, selectedBobaCost, currentBobaObject, isLoading} = this.state;
        const myUrl = "https://bobain.herokuapp.com";

        const resetStates = () => {
            axios.get(`${myUrl}/rest-api/current-boba`)
            .then(res => {
                this.setState({ currentBobaObject : res.data});
            });
            this.setState({ 
                selectedBoba: "Clear Boba",
                selectedBobaCost: 10000,
                selectedToppings : [],
                selectedToppingsCost : 0,
                allSelectedBoba: [],
                currentNumberOfBoba : 1,
                bobaId : 1
            });
        }
    
        const handleChangeNumberOfBoba = (e) => {
            this.setState({ currentNumberOfBoba: e.target.value });
          }
        
        const getImage = (image) => {
            if(image === 'Ice') return ice;
            if(image === 'Cheese') return cheese;
            if(image === 'Brown Sugar') return brown_sugar;
            if(image === 'Black Bubble') return black_bubble;
            if(image === 'Clear Boba') return clear;
            if(image === 'Tea Boba') return tea;
            if(image === 'Milk Boba') return milk;
        }

        const getSelectedToppings = selectedToppings.map(
            (topping) => (
                <div key={topping} className="selected-boba-topping-container">
                    <img className="selected-topping-img" src={getImage(topping)} alt={topping}/>
                    <p className="selected-topping-name">{topping}</p>
                </div>
            )
        );
        
        const selectBoba = (boba) => {
            this.setState({
                selectedBoba: boba
            });
        }

        const selectBobaClear = () => {
            console.log("CHANGED BOBA TO CLEAR");
            selectBoba("Clear Boba");
            this.setState({selectedBobaCost: 10000});
        }

        const selectBobaTea = () => {
            console.log("CHANGED BOBA TO TEA");
            selectBoba("Tea Boba");
            this.setState({selectedBobaCost: 15000});
        }

        const selectBobaMilk = () => {
            console.log("CHANGED BOBA TO MILK");
            selectBoba("Milk Boba");
            this.setState({selectedBobaCost: 17000});
        }

        const addSelectedToppingsCost = (topping) => {
            if(topping === "Ice") this.setState({selectedToppingsCost: this.state.selectedToppingsCost + 3000})
            else this.setState({selectedToppingsCost: this.state.selectedToppingsCost + 5000})
        }
        
        const removeSelectedToppingsCost = (topping) => {
            if(topping === "Ice") this.setState({selectedToppingsCost: this.state.selectedToppingsCost - 3000})
            else this.setState({selectedToppingsCost: this.state.selectedToppingsCost - 5000})
        }

        const selectDeselectTopping = (topping) => {
            if(!selectedToppings.includes(topping)){
                if(selectedToppings.length<3){
                    this.setState({
                        selectedToppings : [...selectedToppings, topping]
                    })
                    addSelectedToppingsCost(topping);
                }
                else{
                    alert("You can only have up to 3 toppings per boba")
                }
            }
            else{
                let newToppings = [...selectedToppings];
                let index = newToppings.indexOf(topping);
                if (index !== -1) {
                    newToppings.splice(index, 1);
                    this.setState({selectedToppings: newToppings});
                    removeSelectedToppingsCost(topping);
                }
            }

        }

        const selectDeselectIce = () => {selectDeselectTopping("Ice")}
        const selectDeselectCheese = () => {selectDeselectTopping("Cheese")}
        const selectDeselectBrownSugar = () => {selectDeselectTopping("Brown Sugar")}
        const selectDeselectBlackBubble = () => {selectDeselectTopping("Black Bubble")}

        const saveBoba = () => {
            if(currentNumberOfBoba >= 1){
                const newBoba = {
                    id: bobaId,
                    type: selectedBoba,
                    toppings: selectedToppings,
                    numberOfBoba: currentNumberOfBoba
                };

                this.setState({
                    isLoading: true,
                    allSelectedBoba: [...allSelectedBoba, newBoba],
                    bobaId: bobaId + 1,
                }, () => {
                    order();
                    setTimeout(() => {
                        this.setState({ isLoading: false });
                        alert("Saved Boba(s) to cart! :)");
                    }, 1500);
                });
            }
        }

        const getDescriptionFromSelectedToppings = () => {
            if(selectedToppings.length === 0) return selectedBoba;
            else if(selectedToppings.length === 1){
                return selectedBoba + " with " + selectedToppings[0];
            }
            else{
                let res = selectedBoba + " with " + selectedToppings[0];
                for(let i=1; i<selectedToppings.length; i++){
                    res += ", " + selectedToppings[i]
                }
                return res;
            }
        }

        const setBobaObjectClear = () => {
            axios.post(`${myUrl}/rest-api/set-boba=clear`)
            .then(res => {
                console.log("SET BOBA TO CLEAR");
            })
        }
        const setBobaObjectTea = () => {
            axios.post(`${myUrl}/rest-api/set-boba=tea`)
            .then(res => {
                console.log("SET BOBA TO TEA");
            })
        }
        const setBobaObjectMilk = () => {
            axios.post(`${myUrl}/rest-api/set-boba=milk`)
            .then(res => {
                console.log("SET BOBA TO MILK");
            })
        }

        const setBobaObject = (boba) => {
            if(boba.type === "Clear Boba"){
                setBobaObjectClear();
            }
            else if(boba.type === "Tea Boba"){
                setBobaObjectTea();
            }
            else if(boba.type === "Milk Boba"){
                setBobaObjectMilk();
            }
        }

        const addToppingToBobaObjectIce = () => {
            axios.post(`${myUrl}/rest-api/add-topping=ice`)
            .then(res => {
                console.log("ADD TOPPING ICE");
            })
        }

        const addToppingToBobaObjectCheese = () => {
            axios.post(`${myUrl}/rest-api/add-topping=cheese`)
            .then(res => {
                console.log("ADD TOPPING CHEESE");
            })
        }

        const addToppingToBobaObjectBrownSugar = () => {
            axios.post(`${myUrl}/rest-api/add-topping=brown-sugar`)
            .then(res => {
                console.log("ADD TOPPING BROWN SUGAR");
            })
        }

        const addToppingToBobaObjectBlackBubble = () => {
            axios.post(`${myUrl}/rest-api/add-topping=black-bubble`)
            .then(res => {
                console.log("ADD TOPPING BLACK BUBBLE");
            })
        }

        const addToppingToBobaObject = (topping) => {
            if(topping === "Ice") addToppingToBobaObjectIce();
            else if(topping === "Cheese") addToppingToBobaObjectCheese();
            else if(topping === "Brown Sugar") addToppingToBobaObjectBrownSugar();
            else if(topping === "Black Bubble") addToppingToBobaObjectBlackBubble();

        }

        const saveBobaObject = (numberOfBoba) => {
            const bodyFormData = new FormData();
            bodyFormData.set('jumlah', numberOfBoba);

            axios({
                method: 'post',
                url: `${myUrl}/rest-api/save`,
                data: bodyFormData
                })
            .then(res => {
                console.log("SAVED BOBA");
            });
        }

        const containsTopping = (topping) => {
            return selectedToppings.indexOf(topping) !== -1;
        }

        const order = () => {
            this.state.allSelectedBoba.forEach(boba => {
                setBobaObject(boba);
                boba.toppings.forEach(topping => {
                    addToppingToBobaObject(topping);
                });
                console.log("TESTTTT ORDER")
                console.log(currentBobaObject)
                saveBobaObject(boba.numberOfBoba);
            });
            resetStates();
        }

        const addOrRemove = (topping) =>{
            let index = selectedToppings.indexOf(topping);
            if (index !== -1) {
                return "Remove";
            }
            return "Add";
        }

        return (
            isLoading ?
            <div className="loading-screen-container">
                <Header></Header>
                <LoadingScreen></LoadingScreen>
            </div>
            :
            <CustomBobaContainer>
                <Header></Header>
            <section className="section">
                <div className="section-title-container">
                    <h1 className="section-title">
                        CUSTOM DRINK
                    </h1>
                </div>
                <div className="section-custom-drink">
                    <div className="selected-base">
                        <h3 className="sub-title">
                            Selected Base
                        </h3>
                        <div className="selected-boba-toppings-container">
                            <img id="selected-base-img" className="selected-base-img" src={getImage(selectedBoba)} alt={selectedBoba}/>
                            <div id="selected-toppings-container" className="selected-toppings">
                               {getSelectedToppings}
                            </div>
                        </div>
                        <div className="selected-base-desc">
                            <p id="selected-base-desc-title">{getDescriptionFromSelectedToppings()}</p>
                            <p id="selected-base-desc-cost">{`Rp ${selectedBobaCost + selectedToppingsCost}`}</p>
                        </div>
                    </div>
                    <div className="options-container">
                        <div className="option-container">
                            <img src={ getImage("Clear Boba") } alt="clear"/>
                            <div className="desc-button-container">
                                <div className="desc">
                                    <p>Clear Boba</p>
                                    <p>Rp 10000</p>
                                </div>
                                <button className="button" onClick={selectBobaClear}>Pick Base</button>
                            </div>
                        </div>
                        <div className="option-container">
                            <img src={getImage("Tea Boba")} alt="tea"/>
                            <div className="desc-button-container">
                                <div className="desc">
                                    <p>Tea Boba</p>
                                    <p>Rp 15000</p>
                                </div>
                                    <button className="button" onClick={selectBobaTea}>Pick Base</button>
                            </div>
                        </div>
                        <div className="option-container">
                            <img src={getImage("Milk Boba")} alt="milk"/>
                            <div className="desc-button-container">
                                <div className="desc">
                                    <p>Milk Boba</p>
                                    <p>Rp 17000</p>
                                </div>
                                <button className="button" onClick={ selectBobaMilk }>Pick Base</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div className="white-line"></div>
            <section className="section">
                <h3 className="sub-title add-toppings-sub-title">Add Topping(s)</h3>
                <div className="section-add-toppings flex-column">
                    <div className="option-container toppings-container">
                        <div className="topping-container">
                            <img src={getImage("Ice")} alt="ice"/>
                            <div className="desc-button-container">
                                <div className="desc">
                                    <p>Ice</p>
                                    <p>Rp 3000</p>
                                </div>
                                <button className={containsTopping("Ice") ? "button red-button" : "button"} onClick={selectDeselectIce}>{addOrRemove("Ice")}</button>
                            </div>
                        </div>
                        <div className="topping-container">
                            <img src={getImage("Cheese")} alt="cheese"/>
                            <div className="desc-button-container">
                                <div className="desc">
                                    <p>Cheese</p>
                                    <p>Rp 5000</p>
                                </div>
                                <button className={containsTopping("Cheese") ? "button red-button" : "button"} onClick={selectDeselectCheese}>{addOrRemove("Cheese")}</button>
                            </div>
                        </div>
                        <div className="topping-container">
                            <img src={getImage("Brown Sugar")} alt="brown-sugar"/>
                            <div className="desc-button-container">
                                <div className="desc">
                                    <p>Brown Sugar</p>
                                    <p>Rp 5000</p>
                                </div>
                                <button className={containsTopping("Brown Sugar") ? "button red-button" : "button"} type="button" onClick={selectDeselectBrownSugar}>{addOrRemove("Brown Sugar")}</button>
                            </div>
                        </div>
                        <div className="topping-container">
                            <img src={getImage("Black Bubble")} alt="black-bubble"/>
                            <div className="desc-button-container">
                                <div className="desc">
                                    <p>Black Bubble</p>
                                    <p>Rp 5000</p>
                                </div>
                                <button className={containsTopping("Black Bubble") ? "button red-button" : "button"} type="button" onClick={selectDeselectBlackBubble}>{addOrRemove("Black Bubble")}</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="save-to-cart-container">
                        <input name="jumlah" value={currentNumberOfBoba} onChange={ handleChangeNumberOfBoba.bind(this) } id="save-to-cart-input" className="save-to-cart-input" type="number" min="1"/>
                        <button className="button" onClick={saveBoba}>Add to cart</button>
                </div>

            </section>
            </CustomBobaContainer>
        );
    }
}

export default CustomBoba;
