import styled from "styled-components";

export const CustomBobaContainer = styled.div`
.add-toppings-sub-title
{
	margin-left: 10%;
	margin-top: 20px;
}
.desc
{
	margin-bottom: 15px;
}
.desc-button-container
{
	margin-left: 15px;
	width: 120px;
}

.red-button{
	background-color: var(--red);
	color: var(--white);
	transition: ease-in-out 0.3s;
}
.option-container
{
	align-items: center;
	display: flex;
	margin: 20px 0px;
}
.option-container img
{
	width: 120px;
}
.save-to-cart-container
{
	display: flex;
	justify-content: center;
	margin-bottom: 100px;
}
.save-to-cart-input
{
	font-size: 1.2em;
	height: 40px;
	margin-right: 10px;
	padding-left: 10px;
	width: 60px;
}
.section-add-toppings
{
	align-items: center;
	display: flex;
	justify-content: center;
	padding: 0px 10% 30px 10%;
}
.section-custom-drink
{
	display: flex;
	justify-content: space-between;
	padding: 50px 20% 30px 10%;
}
.selected-base-img
{
	margin: 20px 0;
	width: 300px;
}
.selected-base-desc
{
    text-align: center;
	font-size: 20px;
}
.selected-boba-toppings-container{
    display: flex;
}
.selected-boba-topping-container
    {
        display: flex;
        align-items: center;
    }   
.selected-topping
{
	align-items: center;
    display: flex;
	font-size: 17px;
}
.selected-topping-img
{
    width: 70px;
    margin: 20px 0px;
	margin-right: 10px;
}
.selected-toppings
{
	display: flex;
    flex-direction: column;
	height: 100%;
    margin-left: 20px;
}
.topping-container
{
	display: flex;
	margin: 20px 0;
}
.toppings-container
{
	display: flex;
	flex-wrap: wrap;
	justify-content: center;
	width: 100%;
}
@media only screen and (max-width: 350px)
{
	.option-container img
	{
		width: 100px;
	}
	.selected-base-img
	{
		margin: 20px 0;
		width: 100%;
	}
	.selected-base-desc
	{
		font-size: 17px;
		font-weight: 600;
	}
}
@media only screen and (max-width: 500px)
{
	.section-custom-drink
	{
		padding: 20px 20% 30px 10%;
	}
	.selected-base-img
	{
		margin: 20px 0;
		width: 80%;
	}
	.selected-base-desc
	{
		font-size: 20px;
        margin-bottom: 50px;
	}
}
@media only screen and (max-width: 950px)
{
	.section-custom-drink
	{
		align-items: center;
		flex-direction: column;
		justify-content: center;
		padding: 50px 10%;
	}
	.selected-base
	{
		align-items: center;
		display: flex;
		flex-direction: column;
    }
    .selected-base-desc
	{
        font-weight: 600;
        margin-bottom: 50px;
    }
	.selected-boba-topping-container{
		flex-direction: column;
	}
    .selected-boba-toppings-container{
        flex-direction: column;
        align-items: center;
		max-width: 90%;
    }
    .selected-topping{
        flex-direction: column;
        justify-content: center;
    }
    .selected-topping-name{
        font-size: 13.5px;
        text-align: center;
        margin: 5px 0px;
    }
    .selected-topping-img{
        margin: 0px 10px;
    }
    .selected-toppings
    {
        flex-direction: row;
        margin-bottom: 30px;
    }
	.sub-title
	{
		text-align: center;
    }
    
}

`