import styled from "styled-components";

export const LoadingScreenContainer = styled.div`

.loading-gif{
	width: 200px;
	margin-top: 40px;
}

.loading-screen{
    width: 100vw;
    height: 100vh;
	background-color: var(--background);
	position: fixed;
	top: 0;
	left: 0;
	display: flex;
	justify-content: center;
	align-items: center;
	flex-direction: column;
	transition: ease-in-out;
}

.not-shown{
	display: none !important;
    transition: ease-in-out;
}

`