import 'bootstrap/dist/css/bootstrap.css';
import React, { Component } from 'react'
import { CartContainer } from './style';
import Header from '../Header';
import LoadingScreenCart from '../LoadingScreenCart';
import axios from 'axios';
import { Link } from "react-router-dom";


export default class Cart extends Component {
    constructor(){
        super();
        this.state = {
            orders : [],
            totalPrice : 0,
            receipt : {},
            isLoading : true
        }

    }
    componentDidMount(){
        const myUrl = "https://bobain.herokuapp.com"

        const loadCart = () => {
            axios.get(`${myUrl}/rest-api/cart/get-orders`)
            .then(result => {
                console.log("TESTTTTTTTTTTTTTT")
                console.log(result.data)
                this.setState({ orders : result.data});
            }
            );
    
            axios.get(`${myUrl}/rest-api/cart/get-total-price`)
            .then(result => {
                this.setState({ totalPrice : result.data});
            }
            );
        }

        const getReceipt = () => {
            axios.get(`${myUrl}/rest-api/cart/get-receipt`)
            .then(result => {
                this.setState({ receipt : result.data});
            }
            );
        }

        loadCart();
        getReceipt();
        setTimeout(() => {
            this.setState({ isLoading: false });
        }, 1500);
    }


    render() {
        const {orders, receipt, isLoading} = this.state;

        const myUrl = "https://bobain.herokuapp.com"

        const loadCart = () => {
            axios.get(`${myUrl}/rest-api/cart/get-orders`)
            .then(result => {
                this.setState({ orders : result.data});
            }
            );
    
            axios.get(`${myUrl}/rest-api/cart/get-total-price`)
            .then(result => {
                this.setState({ totalPrice : result.data});
            }
            );
        }


        const deleteOrder = (index) => {
            const bodyFormData = new FormData();
            bodyFormData.set('index', index);

            axios({
                method: 'post',
                url: `${myUrl}/rest-api/cart/delete`,
                data: bodyFormData
                })
            .then(() => {
                loadCart();
            });
        }
        
        const submitPromo = () => {
            const codeVal = document.getElementById("discount").value;

            const bodyFormData = new FormData();
            bodyFormData.set('code', codeVal);

            axios({
                method: 'post',
                url: `${myUrl}/rest-api/cart/discount`,
                data: bodyFormData
                })
            .then(() => {
                loadCart()
                axios.get(`${myUrl}/rest-api/cart/get-orders`)
                    .then(result => {
                        this.setState({ orders : result.data});
                        axios.get(`${myUrl}/rest-api/cart/get-total-price`)
                        .then(result => {
                            this.setState({ totalPrice : result.data});
                        }
                        );
                    }
                    );
            });
        }

        const clearOrder = () => {
            axios.post(`${myUrl}/rest-api/cart/clear-order`)
            .then(() => {
                loadCart();
            })
        }

        const getOrders = orders.map(
            (row, index) => (
                <tr key={index}>
                    <td>
                        {row.description}
                    </td>
                    <td>
                        {row.price}
                    </td>
                    <td>
                        <button className="btn btn-outline-dark" onClick={() => deleteOrder(index)}>
                            Delete
                        </button>
                    </td>
                </tr>
            )
        );
        
        return (
            isLoading ?
            <div className="loading-screen-container">
                <Header></Header>
                <LoadingScreenCart></LoadingScreenCart>
            </div>
            :
            <CartContainer>
                <Header></Header>
                <section className="section">
                    <div className="section-title-container">
                        <h1 className="section-title">
                            My Cart
                        </h1>
                    </div>
                    <div className="container-fluid">
                        <div className="row">
                            <table className="table table-bordered table-hover" id="order">
                                <thead className="table-header">
                                    <tr>
                                        <th>Order</th>
                                        <th>Price</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody id="order-body">
                                    {getOrders}
                                </tbody>
                            </table>
                            <p id="total-price"></p>
                        </div>
                    </div>
                    <div className="container-fluid">
                        <div className="mt-2">
                            <form>
                                <div className="form-group">
                                    <label for="discount">Enter promo code here:</label>
                                    <input className="form-control" type="text" id="discount"/>
                                </div>
                            </form>
                            <button onClick={submitPromo} className="btn btn-outline-dark" id="discount-button">Submit Promo Code</button>
                        </div>
                        <div className="mt-3">
                            <button className="btn btn-outline-dark" onClick={clearOrder}>Clear order</button>
                        </div>

                        <div className="mt-3">
                            <div className="form-group">
                                <input type="hidden" name="code" value={receipt.id}/>
                            </div>
                            <Link to="/order-code" className="no-decor">
                                <button className="btn btn-outline-dark" type="button">Get your order code!</button>
                            </Link>
                        </div>
                    </div>
                </section>
            </CartContainer>
        )
    }
}
