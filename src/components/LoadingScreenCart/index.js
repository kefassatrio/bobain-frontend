import React, { Component } from 'react';
import {LoadingScreenContainer} from './style';
import loading_gif from '../../assets/img/loading_gif.gif';

class LoadingScreen extends Component {
    render() {
        return (
            <LoadingScreenContainer>
                <div id="loading-screen" className="loading-screen">
                    <h3 className="sub-title">
                        Loading your orders now!
                    </h3>
                    <img className="loading-gif" src={loading_gif} alt="loading"/>
                </div>
            </LoadingScreenContainer>
        );
    }
}

export default LoadingScreen;
