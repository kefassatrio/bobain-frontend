import React, { Component } from 'react'
import {MainMenuContainer} from './style'
import axios from 'axios';
import { Link } from "react-router-dom";
import iceCheeseTea from '../../assets/img/Ice Cheese Tea.png';
import iceBubbleTea from '../../assets/img/Ice Bubble Tea.png';
import iceSweetMilk from '../../assets/img/Ice Sweet Milk.png';
import cheesyBubble from '../../assets/img/Cheesy Bubble.png';
import customBoba from '../../assets/img/Custom Boba.png';
import Header from '../Header';
import LoadingScreen from '../LoadingScreen';

export default class MainMenu extends Component {
    constructor(){
        super();
        this.state = {
            isLoading: false,
        }    
    }

    render() {
        const {isLoading} = this.state;
        const myUrl = "https://bobain.herokuapp.com";

        const saveCheesyBubble = () => {
            this.setState({isLoading : true});
            console.log("Cheesy Bubble");

            axios.post(`${myUrl}/rest-api/save-cheesy-bubble`)
            .then(res => {
                setTimeout(() => {
                    this.setState({isLoading : false});
                    alert("Cheesy Bubble Saved to cart! :)");
                }, 1500);
            })
        }
        
        const saveIceSweetMilk = () => {
            this.setState({isLoading : true});
            console.log("Ice Sweet Milk");

            axios.post(`${myUrl}/rest-api/save-ice-sweet-milk`)
            .then(res => {
                setTimeout(() => {
                    this.setState({isLoading : false});
                    alert("Ice Sweet Milk Saved to cart! :)");
                }, 1500);
            })
        }

        const saveIceBubbleTea = () => {
            this.setState({isLoading : true});
            console.log("Ice Bubble Tea");

            axios.post(`${myUrl}/rest-api/save-ice-bubble-tea`)
            .then(res => {
                setTimeout(() => {
                    this.setState({isLoading : false});
                    alert("Ice Bubble Tea Saved to cart! :)");
                }, 1500);
            })
        }

        const saveIceCheeseTea = () => {
            this.setState({isLoading : true});
            console.log("Ice Cheese Tea");

            axios.post(`${myUrl}/rest-api/save-ice-cheese-tea`)
            .then(res => {
                setTimeout(() => {
                    this.setState({isLoading : false});
                    alert("Ice Cheese Tea Saved to cart! :)");
                }, 1500);
            })
        }

        return (
            isLoading ?
            <div className="loading-screen-container">
                <Header></Header>
                <LoadingScreen></LoadingScreen>
            </div>
            :
            <MainMenuContainer>
                <Header></Header>
            <section className="section">
        <div className="section-title-container">
            <h1 className="section-title">
                PICK YOUR OWN DRINKS
            </h1>
        </div>
        <h3 className="sub-title">Combo Drinks</h3>
        <div className="section-drinks flex-column">
            <div className="option-container drinks-container">
                <div className="drink-container">
                    <img src={iceCheeseTea} alt="ice-cheese-tea"/>
                    <div className="desc-button-container">
                        <div className="desc">
                            <p><b>Ice Cheese Tea</b></p>
                            <p>Tea Boba with Cheese, Ice</p>
                            <p>Rp 23.000</p>
                        </div>
                            <button onClick={saveIceCheeseTea} className="button" type="submit">Add to Cart</button>
                    </div>
                </div>
                <div className="drink-container">
                    <img src={iceBubbleTea} alt="ice-bubble-tea"/>
                    <div className="desc-button-container">
                        <div className="desc">
                            <p><b>Ice Bubble Tea</b></p>
                            <p>Tea Boba with Black Bubble, Ice</p>
                            <p>Rp 23.000</p>
                        </div>
                            <button onClick={saveIceBubbleTea} className="button" type="submit">Add to Cart</button>
                    </div>
                </div>
                <div className="drink-container">
                    <img src={iceSweetMilk} alt="ice-sweet-milk"/>
                    <div className="desc-button-container">
                        <div className="desc">
                            <p><b>Ice Sweet Milk</b></p>
                            <p>Milk Boba with Brown Sugar, Ice</p>
                            <p>Rp 27.000</p>
                        </div>
                            <button onClick={saveIceSweetMilk} className="button" type="submit">Add to Cart</button>
                    </div>
                </div>
                <div className="drink-container">
                    <img src={cheesyBubble} alt="cheesy-bubble"/>
                    <div className="desc-button-container">
                        <div className="desc">
                            <p><b>Cheesy Bubble</b></p>
                            <p>Clear Boba with Black Bubble, Cheese</p>
                            <p>Rp 20.000</p>
                        </div>
                            <button onClick={saveCheesyBubble} className="button" type="submit">Add to Cart</button>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <div className="white-line"></div>

    <section className="section">
        <div className="section-title-container">
            <h1 className="section-title">
                OR... CUSTOMIZE YOUR OWN BOBA
            </h1>
        </div>
        <div className="section-drinks flex-column">
            <div className="option-container toppings-container">
                <div className="drink-container">
                    <img src={customBoba} alt="custom-boba"/>
                    <div className="desc-button-container">
                        <div className="desc">
                            <p>Custom Boba</p>
                            <p>Pick the toppings yourself!</p>
                        </div>
                        <Link to="/custom-boba" className="no-decor">
                            <button className="button" type="submit">Create</button>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    </section>
            </MainMenuContainer>
        )
    }
}
