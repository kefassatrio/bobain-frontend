import styled from "styled-components";

export const OrderCodeContainer = styled.div`
.container{
    position: relative;
    margin-top: 1.5em;
    flex-wrap: wrap;
    text-align: center;
}

.container-code{
    margin: 3em;
    display: flex;
    justify-content: center;
    text-align: center;
    align-items: center;
}

.row{
    margin-top: 1em;
}

img.toko{
    width: 26em;
    margin: 1em;
    border-radius: 5px;
    transition: 0.3s ease;
}

img.toko:hover{
    transform: scale(1.1);
    box-shadow: 3px 7px 15px;
    z-index: 2;
    transition: 0.3s ease;
}

.section-title-container {
    margin: 1em;
}

#fname{
    text-align: center;
    border: 1.5px solid var(--semi-dark);
    padding: 0.3em;
    width: 80%;
    font-size: 2em;
    background-color: var(--background);
}

#changeText{
    font-size:1em;
    /*display: inline-block;*/
    text-align: center;
}

.buttonOrderCode{
        margin: 0 10px;
}

@media only screen and (max-width : 500px){
    img.toko{
        width: 18em;
        margin:2em;
    }

    #fname{
        padding: 0.2em;
        font-size:1.6em;
    }

    #changeText{
        font-size:0.8em;
    }

    .container-code{
        margin: 2em;
    }

    .buttonOrderCode{
        margin-left:10px;
        width: 100px;
        height: 40px;
        font-size:0.7em;
    }
    .buttonConfirm{
        height: 50px;
    }

}

@media only screen and (max-width: 380px){
    img.toko{
        width: 14em;
        margin:1em;
    }

    #fname{
        padding: 0.1em;
        font-size: 1em;
    }

    #changeText{
        font-size:0.5em;
    }

    .container-code{
        margin: 1em;
    }
}
`