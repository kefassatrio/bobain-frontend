import React, { Component } from 'react'
import { OrderCodeContainer } from './style'
import bobain_store from '../../assets/img/BobaIn Boba Store.jpg'
import chatime_store from '../../assets/img/Chatime Boba Store.jpg'
import hophop_store from '../../assets/img/Hop Hop Boba Store.jpg'
import koi_store from '../../assets/img/Koi Boba Store.jpg'
import xing_futang_store from '../../assets/img/Xing Fu Tang Boba Store.jpg'
import Header from '../Header';


import axios from 'axios';

export default class OrderCode extends Component {
    constructor() {
        super();
        this.state = {
            orderCode: '',
            daftarToko: []
        }
    }

    componentDidMount() {
        const myUrl = "https://bobain.herokuapp.com";   
        
            axios.get(`${myUrl}/rest-api/boba-id-cart`)
            .then(res => {
                console.log(res)
                this.setState(
                    { orderCode: res.data }
                )
            })

            axios.get(`${myUrl}/rest-api/store-cart-page`)
            .then(res => {
                console.log(res)
                this.setState(
                    { daftarToko: res.data }
                )
            })

    }

    render() {
        const {orderCode, daftarToko} = this.state;
        const myUrl = "https://bobain.herokuapp.com";   

        const getTokoImage = (toko) => {
            if(toko === "BobaIn Boba Store") return bobain_store;
            if(toko === "Chatime Boba Store") return chatime_store;
            if(toko === "Hop Hop Boba Store") return hophop_store;
            if(toko === "Koi Boba Store") return koi_store;
            if(toko === "Xing Fu Tang Boba Store") return xing_futang_store;
        }

        const getAllToko = daftarToko.map((toko, index) => (
            <img key={index} alt={toko} className="toko" src={`${getTokoImage(toko)}`}/>
        ))

        const copyClipboard = () => {
            const fname = document.getElementById("fname");

            fname.select();
            document.execCommand("Copy");
            document.getElementById("changeText").innerHTML = 'Copied to <br/> Clipboard!';
        }
        
        const confirmPayment = () => {
            axios.post(`${myUrl}/rest-api/cart/store-receipt`)
            .then(() => {
                console.log("payment confirmed");
            })
            alert("Order confirmed :)")
            window.location.replace('/');
        }

        return (
            <OrderCodeContainer>
                <Header></Header>
                <div className="section-title-container order-code-title">
                    <h1 className="section-title">
                        YOUR ORDER CODE IS
                    </h1>
                </div>
                <div class="container-code">
                    <input type="text" id="fname" name="firstname" value={orderCode}/>
                    <button onClick={copyClipboard} className="button buttonOrderCode" id="buttonCopy" >Copy </button>
                    <button onClick={confirmPayment} type="button" className="button buttonOrderCode buttonConfirm">Confirm Payment</button>
                </div>
                <div>
                    <p id="changeText"></p>
                </div>
                <div className="section-title-container">
                    <h1 className="section-title">
                        PROVIDED STORE
                    </h1>
                </div>
                <div  className="container">
                    {getAllToko}
                </div>
            </OrderCodeContainer>
        )
    }
}
