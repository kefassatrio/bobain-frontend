import CustomBoba from './components/CustomBoba';
import MainMenu from './components/MainMenu';
import OrderCode from './components/OrderCode';
import Cart from './components/Cart';

export const routes = [
    {
      component: MainMenu,
      exact: true,
      path: "/"
    },
    {
      component: CustomBoba,
      exact: true,
      path: "/custom-boba"
    },
    {
      component: OrderCode,
      exact: true,
      path: "/order-code"
    },
    {
      component: Cart,
      exact: true,
      path: "/cart"
    },

]