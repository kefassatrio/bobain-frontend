import React, { Component } from 'react'
import './App.css';
import { routes } from "./routes";
import { BrowserRouter, Switch, Route } from "react-router-dom";

export default class App extends Component {
  render() {

    const pages = routes.map(route => (
      <Route
        component={route.component}
        exact={route.exact}
        path={route.path}
        key={route.path}
      />
    ));

    return (
      <div className="App">
        <BrowserRouter>
          <Switch>{pages}</Switch>
        </BrowserRouter>
      </div>
    )
  }
}

